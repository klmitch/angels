// Copyright (c) 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package angels

import "context"

// Waiter describes an interface for objects that can be used to wait
// for an operation to complete.  The [Waiter.Wait] method can be used
// to wait for completion, while [Waiter.WaitContext] can be used for
// an interruptible wait.  Note that Waiter is not intended to be
// thread-safe, and must be called only from a single thread.
type Waiter interface {
	// Wait waits for the operation to complete, without any defined
	// time limit.  It will return immediately if it is called after
	// the operation has been completed.
	Wait()

	// WaitContext is similar to [Waiter.Wait], except that the
	// operation is interruptible through the use of
	// [context.Context].  If interrupted, WaitContext will return the
	// error from the context.  Like [Waiter.Wait], it will return
	// immediately if it is called after the operation has been
	// completed.
	WaitContext(ctx context.Context) error
}

// ClosedWait is an implementation of [Waiter] that can be returned
// when there is nothing to wait for.
type ClosedWait struct{}

// Wait waits for the operation to complete, without any defined time
// limit.  It will return immediately if it is called after the
// operation has been completed.
func (w ClosedWait) Wait() {
}

// WaitContext is similar to [Waiter.Wait], except that the operation
// is interruptible through the use of [context.Context].  If
// interrupted, WaitContext will return the error from the context.
// Like [Waiter.Wait], it will return immediately if it is called
// after the operation has been completed.
func (w ClosedWait) WaitContext(_ context.Context) error {
	return nil
}

// WaitChan is a [Waiter] implementation that waits for a channel to
// be closed.
type WaitChan struct {
	C <-chan struct{} // The channel to wait on
}

// Wait waits for the operation to complete, without any defined time
// limit.  It will return immediately if it is called after the
// operation has been completed.
func (w *WaitChan) Wait() {
	if w.C != nil {
		<-w.C
		w.C = nil
	}
}

// WaitContext is similar to [Waiter.Wait], except that the operation
// is interruptible through the use of [context.Context].  If
// interrupted, WaitContext will return the error from the context.
// Like [Waiter.Wait], it will return immediately if it is called
// after the operation has been completed.
func (w *WaitChan) WaitContext(ctx context.Context) error {
	// Check if it's already done
	if w.C == nil {
		return nil
	}

	// Get the done channel from the context
	done := ctx.Done()
	if done == nil { // not cancelable; wait only on channel
		<-w.C
		w.C = nil
		return nil
	}

	// Wait for one of the channels to be closed
	select {
	case <-w.C: // operation is done
		w.C = nil
		return nil

	case <-done: // context has been canceled
		return ctx.Err()
	}
}
