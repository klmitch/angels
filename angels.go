// Copyright (c) 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package angels

// Hint is a type for hinting to an [Angel] as to the nature of the
// requested stop.  The [UrgentStop] value could be used by an [Angel]
// to indicate that uncompleted tasks should be skipped, but this is
// solely up to the implementation of the [Angel].
type Hint uint8

// Recognized hints for the [Angel.Stop] method.
const (
	NormalStop Hint = iota // Normal stop is requested
	UrgentStop             // Request to stop urgently, skipping incomplete tasks
)

// Angel is an interface describing a task to run in the background.
// This task could be a long-running task, or it could be a task that
// runs for a short period of time, or even one that runs
// periodically; the defining aspect of an angel is that it can be
// started and stopped.
type Angel interface {
	// Name reports the name of the [Angel]; this is used for
	// diagnostics.
	Name() string

	// Start starts an [Angel].  It is passed a [Manager] object which
	// the task, upon exiting, must signal by calling [Manager.Done].
	// Note that Start is expected to run a new goroutine; the task
	// MUST NOT be run within the context of Start.
	Start(man Manager)

	// Stop terminates an [Angel].  It is passed a [Hint], which may
	// be [NormalStop] or [UrgentStop].  The intent of the [Hint] is
	// to enable the [Angel] to perform an expedient stop if it is
	// able or desired, but any [Angel] implementation is free to
	// ignore the [Hint].  The Stop method must return a [Waiter],
	// which the caller can use to wait for the [Angel] to fully exit.
	Stop(hint Hint) Waiter

	// Waiter returns a [Waiter] instance without initiating a stop to
	// the angel.  It is used for the case where the caller wishes to
	// wait for the angel to stop, but does not wish to actually
	// initiate the stop.  Note that calling Waiter after calling
	// [Angel.Stop] results in undefined behavior; an application
	// calling [Angel.Stop] MUST rely on the [Waiter] returned by that
	// method rather than attempting to request a new one by calling
	// Waiter.
	Waiter() Waiter
}

// Initer is an optional interface that may be implemented by an
// [Angel] implementation.  If implemented, the [Initer.Init] method
// will be called prior to calling [Angel.Start].
type Initer interface {
	// Init is called to initialize an [Angel].  It is passed a
	// [Manager] object which, upon exiting, it must signal by calling
	// [Manager.Done].
	Init(man Manager)
}
