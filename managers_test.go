// Copyright (c) 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package angels

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type mockManager struct {
	mockAngel
}

func (m *mockManager) Add(a Angel) {
	m.Called(a)
}

func (m *mockManager) Done(a Angel) {
	m.Called(a)
}

func (m *mockManager) Angels() []Angel {
	args := m.Called()

	return To[[]Angel](args.Get(0))
}

func TestAngelIDBase(t *testing.T) {
	a := &mockAngel{}

	result := angelID(a)

	assert.NotEqual(t, uintptr(0), result)
}

func TestAngelIDDistinct(t *testing.T) {
	a1 := &mockAngel{}
	a2 := &mockAngel{}

	result1 := angelID(a1)
	result2 := angelID(a2)

	assert.NotEqual(t, result1, result2)
}

func TestAngelIDSame(t *testing.T) {
	a := &mockAngel{}

	result1 := angelID(a)
	result2 := angelID(a)

	assert.Equal(t, result1, result2)
}

func TestArchAngelImplementsManager(t *testing.T) {
	assert.Implements(t, (*Manager)(nil), &ArchAngel{})
}

func TestNew(t *testing.T) {
	result := New()

	assert.Equal(t, &ArchAngel{
		angels: map[uintptr]Angel{},
	}, result)
}

func TestArchAngelWaitBase(t *testing.T) {
	parent := &mockManager{}
	obj := &ArchAngel{
		parent: parent,
	}
	done := make(chan struct{})
	parent.On("Done", obj)
	w1 := &mockWaiter{}
	w1.On("Wait")
	w2 := &mockWaiter{}
	w2.On("Wait")

	obj.wait(done, []Waiter{w1, w2})

	select {
	case <-done:
	default:
		assert.Fail(t, "failed to close done channel")
	}
	parent.AssertExpectations(t)
	w1.AssertExpectations(t)
	w2.AssertExpectations(t)
}

func TestArchAngelWaitNoParent(t *testing.T) {
	obj := &ArchAngel{}
	done := make(chan struct{})
	w1 := &mockWaiter{}
	w1.On("Wait")
	w2 := &mockWaiter{}
	w2.On("Wait")

	obj.wait(done, []Waiter{w1, w2})

	select {
	case <-done:
	default:
		assert.Fail(t, "failed to close done channel")
	}
	w1.AssertExpectations(t)
	w2.AssertExpectations(t)
}

func TestArchAngelName(t *testing.T) {
	obj := &ArchAngel{}

	result := obj.Name()

	assert.Equal(t, "archangel", result)
}

func TestArchAngelStart(t *testing.T) {
	parent := &mockManager{}
	a1 := &mockAngel{}
	a2 := &mockAngel{}
	obj := &ArchAngel{
		angels: map[uintptr]Angel{
			angelID(a1): a1,
			angelID(a2): a2,
		},
	}
	a1.On("Start", obj)
	a2.On("Start", obj)

	obj.Start(parent)

	assert.NotNil(t, obj.done)
	assert.Same(t, parent, obj.parent)
	a1.AssertExpectations(t)
	a2.AssertExpectations(t)
}

func TestArchAngelStopBase(t *testing.T) {
	w1 := &mockWaiter{}
	w1.On("Wait")
	a1 := &mockAngel{}
	a1.On("Stop", NormalStop).Return(w1)
	w2 := &mockWaiter{}
	w2.On("Wait")
	a2 := &mockAngel{}
	a2.On("Stop", NormalStop).Return(w2)
	w3 := &mockWaiter{}
	w3.On("Wait")
	a3 := &mockAngel{}
	a3.On("Stop", NormalStop).Return(w3)
	done := make(chan struct{})
	obj := &ArchAngel{
		done: done,
		angels: map[uintptr]Angel{
			angelID(a1): a1,
			angelID(a2): a2,
			angelID(a3): a3,
		},
	}

	result := obj.Stop(NormalStop)
	<-done

	assert.Equal(t, &WaitChan{
		C: done,
	}, result)
	assert.Nil(t, obj.done)
	a1.AssertExpectations(t)
	a2.AssertExpectations(t)
	a3.AssertExpectations(t)
}

func TestArchAngelStopNotRunning(t *testing.T) {
	a1 := &mockAngel{}
	a2 := &mockAngel{}
	a3 := &mockAngel{}
	obj := &ArchAngel{
		angels: map[uintptr]Angel{
			angelID(a1): a1,
			angelID(a2): a2,
			angelID(a3): a3,
		},
	}

	result := obj.Stop(NormalStop)

	assert.Equal(t, ClosedWait{}, result)
	assert.Nil(t, obj.done)
	a1.AssertExpectations(t)
	a2.AssertExpectations(t)
	a3.AssertExpectations(t)
}

func TestArchAngelWaiterBase(t *testing.T) {
	done := make(chan struct{})
	obj := &ArchAngel{
		done: done,
	}

	result := obj.Waiter()

	assert.Equal(t, &WaitChan{
		C: done,
	}, result)
}

func TestArchAngelWaiterNotRunning(t *testing.T) {
	obj := &ArchAngel{}

	result := obj.Waiter()

	assert.Equal(t, ClosedWait{}, result)
}

func TestArchAngelAddBase(t *testing.T) {
	a := &mockAngel{}
	id := angelID(a)
	done := make(chan struct{})
	obj := &ArchAngel{
		done:   done,
		angels: map[uintptr]Angel{},
	}
	a.On("Start", obj)

	obj.Add(a)

	assert.Equal(t, map[uintptr]Angel{
		id: a,
	}, obj.angels)
	a.AssertExpectations(t)
}

func TestArchAngelAddIdempotent(t *testing.T) {
	a := &mockAngel{}
	id := angelID(a)
	done := make(chan struct{})
	obj := &ArchAngel{
		done: done,
		angels: map[uintptr]Angel{
			id: a,
		},
	}

	obj.Add(a)

	assert.Equal(t, map[uintptr]Angel{
		id: a,
	}, obj.angels)
	a.AssertExpectations(t)
}

func TestArchAngelAddNotRunning(t *testing.T) {
	a := &mockAngel{}
	id := angelID(a)
	obj := &ArchAngel{
		angels: map[uintptr]Angel{},
	}

	obj.Add(a)

	assert.Equal(t, map[uintptr]Angel{
		id: a,
	}, obj.angels)
	a.AssertExpectations(t)
}

func TestArchAngelAddIniter(t *testing.T) {
	a := &mockIniter{}
	id := angelID(a)
	obj := &ArchAngel{
		angels: map[uintptr]Angel{},
	}
	a.On("Init", obj)

	obj.Add(a)

	assert.Equal(t, map[uintptr]Angel{
		id: a,
	}, obj.angels)
	a.AssertExpectations(t)
}

func TestArchAngelDoneBase(t *testing.T) {
	a := &mockAngel{}
	obj := &ArchAngel{
		angels: map[uintptr]Angel{
			angelID(a): a,
		},
	}

	obj.Done(a)

	assert.Equal(t, map[uintptr]Angel{}, obj.angels)
}

func TestArchAngelDoneIdempotent(t *testing.T) {
	a := &mockAngel{}
	obj := &ArchAngel{
		angels: map[uintptr]Angel{},
	}

	obj.Done(a)

	assert.Equal(t, map[uintptr]Angel{}, obj.angels)
}

func TestArchAngelAngels(t *testing.T) {
	a1 := &mockAngel{}
	a2 := &mockAngel{}
	a3 := &mockAngel{}
	obj := &ArchAngel{
		angels: map[uintptr]Angel{
			angelID(a1): a1,
			angelID(a2): a2,
			angelID(a3): a3,
		},
	}

	result := obj.Angels()

	assert.ElementsMatch(t, []Angel{a1, a2, a3}, result)
}
