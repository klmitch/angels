// Copyright (c) 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package angels

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type mockWaiter struct {
	mock.Mock
}

func (m *mockWaiter) Wait() {
	m.Called()
}

func (m *mockWaiter) WaitContext(ctx context.Context) error {
	args := m.Called(ctx)

	return args.Error(0)
}

func TestClosedWaitImplementsWaiter(t *testing.T) {
	assert.Implements(t, (*Waiter)(nil), &ClosedWait{})
}

func TestClosedWaitWait(_ *testing.T) {
	obj := &ClosedWait{}

	obj.Wait()
}

func TestClosedWaitWaitContext(t *testing.T) {
	ctx := context.Background()
	obj := &ClosedWait{}

	err := obj.WaitContext(ctx)

	assert.NoError(t, err)
}

func TestWaitChanImplementsWaiter(t *testing.T) {
	assert.Implements(t, (*Waiter)(nil), &WaitChan{})
}

func TestWaitChanWaitBase(t *testing.T) {
	c := make(chan struct{})
	close(c)
	obj := &WaitChan{
		C: c,
	}

	obj.Wait()

	assert.Nil(t, obj.C)
}

func TestWaitChanWaitClosed(t *testing.T) {
	obj := &WaitChan{}

	obj.Wait()

	assert.Nil(t, obj.C)
}

func TestWaitChanWaitContextBase(t *testing.T) {
	ctx, cancelFn := context.WithCancel(context.Background())
	defer cancelFn()
	c := make(chan struct{})
	close(c)
	obj := &WaitChan{
		C: c,
	}

	err := obj.WaitContext(ctx)

	assert.NoError(t, err)
	assert.Nil(t, obj.C)
}

func TestWaitChanWaitContextClosed(t *testing.T) {
	ctx, cancelFn := context.WithCancel(context.Background())
	defer cancelFn()
	obj := &WaitChan{}

	err := obj.WaitContext(ctx)

	assert.NoError(t, err)
	assert.Nil(t, obj.C)
}

func TestWaitChanWaitContextUncancelable(t *testing.T) {
	ctx := context.Background()
	c := make(chan struct{})
	close(c)
	obj := &WaitChan{
		C: c,
	}

	err := obj.WaitContext(ctx)

	assert.NoError(t, err)
	assert.Nil(t, obj.C)
}

func TestWaitChanWaitContextCanceled(t *testing.T) {
	ctx, cancelFn := context.WithCancel(context.Background())
	cancelFn()
	c := make(chan struct{})
	obj := &WaitChan{
		C: c,
	}

	err := obj.WaitContext(ctx)

	assert.ErrorIs(t, err, context.Canceled)
	assert.NotNil(t, obj.C)
	close(c)
}
