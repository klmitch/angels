// Copyright (c) 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package angels

import (
	"github.com/stretchr/testify/mock"
)

type mockAngel struct {
	mock.Mock
}

func (m *mockAngel) Name() string {
	args := m.Called()

	return args.String(0)
}

func (m *mockAngel) Start(man Manager) {
	m.Called(man)
}

func (m *mockAngel) Stop(hint Hint) Waiter {
	args := m.Called(hint)

	return To[Waiter](args.Get(0))
}

func (m *mockAngel) Waiter() Waiter {
	args := m.Called()

	return To[Waiter](args.Get(0))
}

type mockIniter struct {
	mockAngel
}

func (m *mockIniter) Init(man Manager) {
	m.Called(man)
}
