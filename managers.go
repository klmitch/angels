// Copyright (c) 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package angels

import (
	"reflect"
	"sync"
)

// Manager is an interface implemented by objects that manage groups
// of angels, such as [ArchAngel].
type Manager interface {
	Angel

	// Add adds an [Angel] to the [Manager], starting it if the
	// [Manager] is in the running state.
	Add(a Angel)

	// Done must be called when the [Angel] exits.  The idiomatic way
	// is to call "defer man.Done(a)" at the beginning of the
	// [Angel.Start] method.  Calling this method will detach the
	// [Angel] from the [Manager].
	Done(a Angel)

	// Angels returns a list of all of the running [Angel] instances.
	// Note that the list is not sorted in any way, and could be
	// returned in a different order if called again.
	Angels() []Angel
}

// angelID returns the ID of an [Angel] for the purposes of
// [ArchAngel].  The ID is simply the integer value of the pointer to
// the [Angel]; it is treated as opaque by [ArchAngel], so this is
// safe.
func angelID(a Angel) uintptr {
	return uintptr(reflect.ValueOf(a).UnsafePointer())
}

// ArchAngel is an implementation of [Manager].  It tracks all running
// [Angel] instances, starts new [Angel] instances, and can be used to
// terminate all managed running [Angel] instances.  In short, it is
// in charge.  Users of this library should allocate one of these
// objects.
type ArchAngel struct {
	sync.Mutex

	done   chan struct{}     // Channel for signaling completion
	angels map[uintptr]Angel // Set of running angels
	parent Manager           // Parent manager object
}

// New constructs a new [ArchAngel] object and returns it.
func New() *ArchAngel {
	return &ArchAngel{
		angels: map[uintptr]Angel{},
	}
}

// wait is a helper goroutine that waits for all the provided waiters
// to complete.  Once they have, it closes the done channel, signaling
// anything waiting on the [ArchAngel] object.
func (a *ArchAngel) wait(done chan struct{}, waiters []Waiter) {
	defer close(done)
	if a.parent != nil {
		defer a.parent.Done(a)
	}

	// Wait on the waiters
	for _, waiter := range waiters {
		waiter.Wait()
	}
}

// Name reports the name of the [Angel]; this is used for diagnostics.
func (a *ArchAngel) Name() string {
	return "archangel"
}

// Start starts an [Angel].  It is passed a [Manager] object which the
// task, upon exiting, must signal by calling [Manager.Done].  Note
// that Start is expected to run a new goroutine; the task MUST NOT be
// run within the context of Start.
func (a *ArchAngel) Start(man Manager) {
	a.Lock()
	defer a.Unlock()

	// Save the parent, set ourselves running
	a.parent = man
	a.done = make(chan struct{})

	// Start all the pending angels
	for _, child := range a.angels {
		child.Start(a)
	}
}

// Stop terminates an [Angel].  It is passed a [Hint], which may be
// [NormalStop] or [UrgentStop].  The intent of the [Hint] is to
// enable the [Angel] to perform an expedient stop if it is able or
// desired, but any [Angel] implementation is free to ignore the
// [Hint].  The Stop method must return a [Waiter], which the caller
// can use to wait for the [Angel] to fully exit.
func (a *ArchAngel) Stop(hint Hint) Waiter {
	a.Lock()
	defer a.Unlock()

	if a.done != nil {
		// Save the done channel
		done := a.done
		a.done = nil

		// Walk through the angels and call their Stop methods
		waiters := make([]Waiter, 0, len(a.angels))
		for _, child := range a.angels {
			waiters = append(waiters, child.Stop(hint))
		}

		// Kick off the wait goroutine
		go a.wait(done, waiters)

		// Return a waiter waiting on the done channel
		return &WaitChan{
			C: done,
		}
	}

	return ClosedWait{}
}

// Waiter returns a [Waiter] instance without initiating a stop to the
// angel.  It is used for the case where the caller wishes to wait for
// the angel to stop, but does not wish to actually initiate the stop.
// Note that calling Waiter after calling [Angel.Stop] results in
// undefined behavior; an application calling [Angel.Stop] MUST rely
// on the [Waiter] returned by that method rather than attempting to
// request a new one by calling Waiter.
func (a *ArchAngel) Waiter() Waiter {
	a.Lock()
	defer a.Unlock()

	if a.done != nil {
		return &WaitChan{
			C: a.done,
		}
	}

	return ClosedWait{}
}

// Add adds an [Angel] to the [Manager], starting it if the [Manager]
// is in the running state.
func (a *ArchAngel) Add(child Angel) {
	a.Lock()
	defer a.Unlock()

	// First, get the ID of the angel
	id := angelID(child)

	// If it's already in the set, do nothing further
	if _, ok := a.angels[id]; ok {
		return
	}

	// Save it
	a.angels[id] = child

	// Initialize the child if it's initializable
	if initer, ok := child.(Initer); ok {
		initer.Init(a)
	}

	// Start it if we're running
	if a.done != nil {
		child.Start(a)
	}
}

// Done must be called when the [Angel] exits.  The idiomatic way is
// to call "defer man.Done(a)" at the beginning of the [Angel.Start]
// method.  Calling this method will detach the [Angel] from the
// [Manager].
func (a *ArchAngel) Done(child Angel) {
	a.Lock()
	defer a.Unlock()

	// First, get the ID of the angel
	id := angelID(child)

	// If it's not in the set, do nothing further
	if _, ok := a.angels[id]; !ok {
		return
	}

	// Remove it from the set
	delete(a.angels, id)
}

// Angels returns a list of all of the running [Angel] instances.
// Note that the list is not sorted in any way, and could be returned
// in a different order if called again.
func (a *ArchAngel) Angels() []Angel {
	a.Lock()
	defer a.Unlock()

	// Build the list of angels
	angels := make([]Angel, 0, len(a.angels))
	for _, child := range a.angels {
		angels = append(angels, child)
	}

	return angels
}
